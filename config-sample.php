<?php
	DEFINE['SMTP_USERNAME'] = 'example@example.com';
	DEFINE['SMTP_PASSWORD'] = 'password';
	DEFINE['SMTP_HOST'] 	= 'mail.hostserver.com';
	DEFINE['SMTP_AUTH'] 	= true;
	DEFINE['SMTP_SECURE'] 	= 'ssl';
	DEFINE['SMTP_PORT'] 	= 465;
	
	$config = (object) array(
		'mail' => (object) array(
			'From' => 'example@example.com',
			'FromName' => 'Joe Blogs',
			'addAddress' => array(
				(object) array( 'email' => 'example@example.com', 'name' => 'Joe Blogs 1' ),
				(object) array( 'email' => 'example@example.com2', 'name' => 'Joe Blogs 2' )
			),
			'addReplyTo' => (object) array( 'email' => 'example@example.com', 'name' => 'Joe Blogs' )
		)
	);
	
	
?>