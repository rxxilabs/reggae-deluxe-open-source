var app = angular.module('mainApp', []);

app.controller('gallery', function( $scope ) {

	$scope.showDialog = true;

});

app.controller('mailinglist', function ($scope, $http ) {

	var formData = {
		email: ""
	}
	
	$scope.contactLoader = false; // Loader will not appear for the contact form

	$scope.submitForm = function() {
		angular.extend(formData, $scope.form);
	}
	
	$scope.submitForm = function() {

		angular.extend(formData, $scope.form);
		
		// Validate Form
		
		var errorMessage = [];
		
		if (formData.email === '') {
			errorMessage.push('email')
		}
		
		var message = '';
		
		for ( var i = 0; i < errorMessage.length; i++) {
		
			if (i == 0)
				message += "Please complete your "
			
			if (i == (errorMessage.length -2 ))
				message += errorMessage[i] + ' and ';
			else if (i == (errorMessage.length -1 ))
				message += errorMessage[i];
			else
				message += errorMessage[i] + ', ';
		}
		
		if (errorMessage.length !== 0) {
		
			if (!Modernizr.input.required) {
				$scope.messages = message;
				$scope.alertDangerShow = true;
				$scope.alertSuccessShow = false;
			}
			
			$scope.contactLoader = false;
			return;
		}
		
		$scope.contactLoader = true;
			
		$scope.alertDangerShow = false;
		
		$scope.alertSuccessShow = false;
		
		// If all is valid then...
		
		var fd = new FormData();
		
		angular.forEach(formData, function(value, key) {
			fd.append(key, value);
		});
		
		if (!("FormData" in window)) {
			alert("Your post currently cannot be submitted. Please send the message as you would usually do by email."); // FormData is not supported; degrade gracefully/ alert the user as appropriate
			$scope.contactLoader = false;
			return;
		}
			
		$http.post('api/v1/mail/mailing-list', fd, 
			{ headers: { 'Content-Type': undefined},
			transformRequest: angular.identity })
		.success(function (result) {
		
			// Clear the form
			
			$scope.form.name = '';
			$scope.form.email = '';
			$scope.form.message = '';
			
			// Display success message
		
			$scope.messages = result.message;
			$scope.alertSuccessShow = true;
			
			// Turn off loader again
			
			$scope.contactLoader = false;
			
		})
		.error(function(result) {
		
			// Display error message
		
			$scope.messages = result.message;
			$scope.alertDangerShow = true;
		
			// Turn off loader again
		
			$scope.contactLoader = false;
			
		});
			
	}

});

app.controller('contactform', function ($scope, $http ) {

	var formData = {
		name: "",
		email: "",
		message: ""
	}
	
	$scope.contactLoader = false; // Loader will not appear for the contact form
	
	$scope.submitForm = function() {

		angular.extend(formData, $scope.form);
		
		// Validate Form
		
		var errorMessage = [];
		
		if (formData.name === '') {
			errorMessage.push('name')
		}
		
		if (formData.email === '') {
			errorMessage.push('email')
		}
		
		if (formData.message === '') {
			errorMessage.push('message')
		}
		
		var message = '';
		
		for ( var i = 0; i < errorMessage.length; i++) {
		
			if (i == 0)
				message += "Please complete your "
			
			if (i == (errorMessage.length -2 ))
				message += errorMessage[i] + ' and ';
			else if (i == (errorMessage.length -1 ))
				message += errorMessage[i];
			else
				message += errorMessage[i] + ', ';
		}
		
		if (errorMessage.length !== 0) {
		
			if (!Modernizr.input.required) {
				$scope.messages = message;
				$scope.alertDangerShow = true;
				$scope.alertSuccessShow = false;
			}
			
			$scope.contactLoader = false;
			return;
		}
	
		$scope.contactLoader = true;
			
		$scope.alertDangerShow = false;
		
		$scope.alertSuccessShow = false;
		
		// If all is valid then...
		
		var fd = new FormData();
		
		angular.forEach(formData, function(value, key) {
			fd.append(key, value);
		});
		
		if (!("FormData" in window)) {
			alert("Your post currently cannot be submitted. Please send the message as you would usually do by email."); // FormData is not supported; degrade gracefully/ alert the user as appropriate
			$scope.contactLoader = false;
			return;
		}
			
		$http.post('api/v1/mail/contact', fd, 
			{ headers: { 'Content-Type': undefined},
			transformRequest: angular.identity })
		.success(function (result) {
		
			// Clear the form
			
			$scope.form.name = '';
			$scope.form.email = '';
			$scope.form.message = '';
			
			// Display success message
		
			$scope.messages = result.message;
			$scope.alertSuccessShow = true;
			
			// Turn off loader again
			
			$scope.contactLoader = false;
			
		})
		.error(function(result) {
		
			// Display error message
		
			$scope.messages = result.message;
			$scope.alertDangerShow = true;
		
			// Turn off loader again
		
			$scope.contactLoader = false;
			
		});
			
	}
	
	

});