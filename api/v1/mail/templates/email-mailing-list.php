<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />

    <title>Contact Form Alert</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
</head>

<body>

	<table style="max-width: 560px;font-size: 14px; font-family: Arial, sans-serif;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td colspan="2">
				Hello Reggae Deluxe,
				
				<br/><br/>
				
				Someone has requested to join your mailing list. However, do contact them, and make sure they haven't subscribed accidentally.
				<br/>
				
				<h3>New Subscriber's Details</h3>
				
				<hr/>
				
				<br/>
				
			</td>
		</tr>
		<tr bgcolor="gainsboro">
			<td valign="top" style="padding-left: 20px; padding-bottom: 20px;padding-top: 20px;">&nbsp;<strong>Email:</strong>&nbsp;</td>
			<td valign="top" style="padding-left: 20px; padding-bottom: 20px;padding-top: 20px;"><?php echo $email; ?></td>
		</tr>
		<tr>
			<td valign="top" style="padding-left: 20px; padding-bottom: 20px;padding-top: 20px;">&nbsp;<strong>City:</strong>&nbsp;</td>
			<td valign="top" style="padding-left: 20px; padding-bottom: 20px;padding-top: 20px;"><?php echo $city; ?></td>
		</tr>
		<tr bgcolor="gainsboro">
			<td valign="top" style="padding-left: 20px; padding-bottom: 20px;padding-top: 20px;">&nbsp;<strong>Region:</strong>&nbsp;</td>
			<td valign="top" style="padding-left: 20px; padding-bottom: 20px;padding-top: 20px;"><?php echo $region; ?></td>
		</tr>
		<tr>
			<td valign="top" style="padding-left: 20px; padding-bottom: 20px;padding-top: 20px;">&nbsp;<strong>Country:</strong>&nbsp;</td>
			<td valign="top" style="padding-left: 20px; padding-bottom: 20px;padding-top: 20px;"><?php echo $country; ?></td>
		</tr>
		<tr>
			<td colspan="2">
				<br/><br/>
				
				From The Auto Contacter on ReggaeDeluxe.com
				
				<br/><br/>
				
				<i>PS. Contact gblackuk@googlemail.com if there are any problems</i>
				
			</td>
		</tr>
	</table>

</body>
</html>