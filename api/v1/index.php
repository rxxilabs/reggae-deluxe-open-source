<?php

	include  $_SERVER['DOCUMENT_ROOT'] . '/config.php';
	require '../../libs/Slim/Slim.php';
	\Slim\Slim::registerAutoloader();
	
	$app = new \Slim\Slim();
	
	class json {
	
		const NAME = 'json';
	
		public static function cors() {

			// Allow from any origin
			if (isset($_SERVER['HTTP_ORIGIN'])) {
				header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
				header('Access-Control-Allow-Credentials: true');
				header('Access-Control-Max-Age: 86400');    // cache for 1 day
			}

			// Access-Control headers are received during OPTIONS requests
			if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

				if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
					header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

				if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
					header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

				exit(0);
			}

			//echo "You have CORS!";
		}
		
	}
	
	$app->get('/', function() use ($app) {
		
		echo "No content here.";
	});
	
	$app->post('/mail/contact', function() use ($app) {
	
		// Turn on cors
		json::cors();
				
		$paramValue = $app->request()->post();
		
		// No headers are sent
		
		if (count($paramValue) == 0) {
			echo json_encode(array( 
				'status' => 'fail', 
				'data' => array(
					'message' => 'A user message is required',
					'email' => 'An email address is required'
				)));
				
			return;
		}
		
		/** ======= Anti-Robot Email ======= **/
		
		if (!empty($paramValue['emailaddress'])) {
			error_log($_SERVER['HTTP_HOST'] . ": Warning Contact Form attempt populating css hidden field from IP Address: " . $_SERVER['REMOTE_ADDR'], 0);
			exit;
		}
		
		/** ======= Error Messages ======= **/
		
		$errorMessages = (object) array();
		
		// Email Validation

		if (empty($paramValue['email']))
			$errorMessages->email = 'Email address is empty';
		
		else if (!filter_var($paramValue['email'], FILTER_VALIDATE_EMAIL))
			$errorMessages->email = 'Email address is invalid';
			
		// Message Validation
		
		if (empty($paramValue['message']))
			$errorMessages->message = 'Message cannot be empty';
			
		// Name Validation
		
		if (empty($paramValue['name']))
			$errorMessages->name = 'Name cannot be empty';
			
		// If there are any error messages	
		
		if(count((array)$errorMessages) > 0){
			echo json_encode(array( 
					'status' => 'fail', 
					'data' => $errorMessages));
					
				return;
				
		}
		
		// Setup Email To Send
		
		include $_SERVER['DOCUMENT_ROOT'] . '/libs/PHPMailer/PHPMailerAutoload.php';
		
		$mail = new PHPMailer;
		
		//$mail->SMTPDebug  = 1;
		
		$mail->isSMTP();                       
		$mail->Host 		= SMTP_HOST;  
		$mail->SMTPAuth 	= SMTP_AUTH;                 
		$mail->Username 	= SMTP_USERNAME;            
		$mail->Password 	= SMTP_PASSWORD;            
		$mail->SMTPSecure 	= SMTP_SECURE;
		$mail->Port       	= SMTP_PORT;   
		
		$mail->From = $config->mail->From;
		$mail->FromName = $config->mail->FromName;
		
		foreach ($config->mail->addAddress as $to) {
			$mail->addAddress($to->email, $to->name);
		}
		
		$mail->addReplyTo( $config->mail->addReplyTo->email, $config->mail->addReplyTo->name);

		$mail->isHTML(true);
		
		$view = $app->view();
		$view->setTemplatesDirectory('./mail/templates/');
		$view->appendData(array( 
			'name' => $paramValue['name'],
			'email' => $paramValue['email'],
			'message' => $paramValue['message']
		));
		
		$tpl = $view->fetch( 'email-contact.php' );
		
		$mail->Subject = 'Reggae Deluxe New Message';
		$mail->Body    = $tpl;
		$mail->AltBody = $paramValue['name'].'\n\r\n\r'. $paramValue['email'].'\n\r\n\r'. $paramValue['message'].'\n\r\n\r';
		
		if(!$mail->send()) {
		   echo json_encode(array( 
					'status' => 'error', 
					'message' => 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo));
					
				return;
		}
		
		echo json_encode(array( 
					'status' => 'success', 
					'message' => 'Thank you for contacting Reggae Deluxe. Your message has been sent successfully and you will be contacted shortly.'));
					
				return;

	});
	
	$app->post('/mail/mailing-list', function() use ($app) {
	
		json::cors();
		
		$paramValue = $app->request()->post();
		
		// No headers are sent
		
		if (count($paramValue) == 0) {
			echo json_encode(array( 
				'status' => 'fail', 
				'data' => array(
					'email' => 'An email address is required'
				)));
				
			return;
		}
		
		/** ======= Anti-Robot Email ======= **/
		
		if (!empty($paramValue['emailaddress'])) {
			error_log($_SERVER['HTTP_HOST'] . ": Warning Mailing List attempt populating css hidden field from IP Address: " . $_SERVER['REMOTE_ADDR'], 0);
			exit;
		}
		
		/** ======= Error Messages ======= **/
		
		$errorMessages = (object) array();
		
		// Email Validation

		if (empty($paramValue['email']))
			$errorMessages->email = 'Email address is empty';
		
		else if (!filter_var($paramValue['email'], FILTER_VALIDATE_EMAIL))
			$errorMessages->email = 'Email address is invalid';
			
		/** ======= Default Variables ======= **/
		
		$email 		= $paramValue['email'];
		$city 		= '';
		$region 	= '';
		$country 	= '';
		
		/** ======= Get IP Info & Location ======= **/
		
		$ip = $_SERVER['REMOTE_ADDR'];
		
		if ($ip != '') {
			$details 	= json_decode(file_get_contents('http://freegeoip.net/json/92.4.135.141'));
			$city 		= (isset($details->city)) ? $details->city : '';
			$region 	= (isset($details->region_name)) ? $details->region_name : '';
			$country 	= (isset($details->country_name)) ? $details->country_name : '';
		}
		
		// Setup Email To Send
		
		require '../../libs/PHPMailer/PHPMailerAutoload.php';
		
		$mail = new PHPMailer;
		
		$mail->isSMTP();                       
		$mail->Host 		= SMTP_HOST;  
		$mail->SMTPAuth 	= SMTP_AUTH;                 
		$mail->Username 	= SMTP_USERNAME;            
		$mail->Password 	= SMTP_PASSWORD;            
		$mail->SMTPSecure 	= SMTP_SECURE;
		$mail->Port       	= SMTP_PORT;   
		
		$mail->From = $config->mail->From;
		$mail->FromName = $config->mail->FromName;
		
		foreach ($config->mail->addAddress as $to) {
			$mail->addAddress($to->email, $to->name);
		}
		
		$mail->addReplyTo( $config->mail->addReplyTo->email, $config->mail->addReplyTo->name);

		$mail->isHTML(true);
		
		$view = $app->view();
		$view->setTemplatesDirectory('./mail/templates/');
		$view->appendData(array( 
			'email' 	=> $email,
			'city' 		=> $city,
			'region' 	=> $region,
			'country' 	=> $country,
		));
		
		$tpl = $view->fetch( 'email-mailing-list.php' );
		
		$mail->Subject = 'Reggae Deluxe New Subscriber';
		$mail->Body    = $tpl;
		$mail->AltBody = "Email: ".$email.'\n\r\n\r'."City: ".$city.'\n\r\n\r'."Region: ".$region.'\n\r\n\r'."Country: ".$country;
		
		if(!$mail->send()) {
		   echo json_encode(array( 
					'status' => 'error', 
					'message' => 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo));
					
				return;
		}
		
		echo json_encode(array( 
					'status' => 'success', 
					'message' => 'Thank you for subscribing to the mailing list. Reggae Deluxe will keep in touch. Feel free to send the band a message too.'));
					
				return;
		
	});

	$app->run();

?>